const gallery = document.querySelector('.tabs');

gallery.addEventListener('click', event =>{
    if(event.target.classList.contains('tab-item')){
        document.querySelectorAll('.tab-item').forEach(item => item.classList.remove('active-tab'));
        event.target.classList.add('active-tab');
        document.querySelectorAll('.img-text').forEach(item => item.classList.remove('active-tab-content'));
        document.querySelector(`.${event.target.textContent.toLowerCase().replace(/\s+/g,'')}`).classList.add('active-tab-content')
    } 
})

const workTabsBtn = document.querySelectorAll('.work-tab-item');

workTabsBtn.forEach(function(item){
    item.addEventListener('click', function(event){
        workTabsBtn.forEach(function(item){
            item.classList.remove('work-active-tab')
            event.target.classList.add('work-active-tab')   
            let imagesTestCont = document.querySelectorAll('.img-wrapper');
            imagesTestCont.forEach(function(item){
                if(event.target.dataset.title === item.dataset.content){
                    item.style.display = 'block';               
                }else if(event.target.dataset.title === 'all'){
                    item.style.display = 'block'
                }else{
                    item.style.display = 'none'
                }
            })        
        })
    })
})

let container = document.querySelector('.container');

for(let i = 1; i <= 3; i++){
    container.insertAdjacentHTML('afterbegin', `<div class="img-wrapper" data-content="graphic_design"><img class="success" src="./IMG/graphic design/graphic-design${i}.jpg"></div>`)
    container.insertAdjacentHTML('afterbegin', `<div class="img-wrapper" data-content="web_design"><img src="./IMG/web design/web-design${i}.jpg"></class=>`)
    container.insertAdjacentHTML('afterbegin', `<div class="img-wrapper" data-content="landing_pages"><img src="./IMG/landing page/landing-page${i}.jpg"></div>`)
    container.insertAdjacentHTML('afterbegin', `<div class="img-wrapper" data-content="wordpress"><img src="./IMG/wordpress/wordpress${i}.jpg"></div>`)
}

let loader = document.querySelector('.loader')
let loadButton = document.querySelector('.load-more-button');
loadButton.addEventListener('click', (event)=> {
    event.preventDefault();
    loader.classList.remove('hidden-loader')
    event.target.parentNode.replaceChild(document.querySelector('.loader'), loadButton);
    setTimeout(function(){
        loader.classList.add('hidden-loader')     
        for(let i = 4; i <= 6; i++){
    container.insertAdjacentHTML('beforeend', `<div class="img-wrapper" data-content="graphic_design"><img class="success" src="./IMG/graphic design/graphic-design${i}.jpg"></div>`)
    container.insertAdjacentHTML('beforeend', `<div class="img-wrapper" data-content="web_design"><img src="./IMG/web design/web-design${i}.jpg"></div>`)
    container.insertAdjacentHTML('beforeend', `<div class="img-wrapper" data-content="landing_pages"><img src="./IMG/landing page/landing-page${i}.jpg"></div>`)
    container.insertAdjacentHTML('beforeend', `<div class="img-wrapper" data-content="wordpress"><img src="./IMG/wordpress/wordpress${i}.jpg"></div>`)
} 
hoverImages();
    },2000)
})

function hoverImages(){
    let imageContainer = document.querySelectorAll('.img-wrapper');
    imageContainer.forEach(item => item.addEventListener('mouseenter', function(event){
        if(event.target.classList.contains('img-wrapper')){
            event.target.insertAdjacentHTML('beforeend', `<div id="replacement" class="replacementblock">
            <div class="icons"><a href="#" class="link icon-link link-chain">
            <svg class="chain-svg" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/></svg>
            </a><a href="#" class="link icon-link link-square"></a></div><a href="#" class="link"><h3 class="icon-headline">creative design</h3>
            </a><a href="#" class="link">
            <h4 class="icon-subheading">${event.target.dataset.content.replace('_', ' ')}</h4></a></div>`)
        }
        imageContainer.forEach(item => item.addEventListener('mouseleave', function(){
            document.querySelector('.replacementblock').remove()
        }))
    }))
}
hoverImages();

//SLICK SLIDER//
$('.rev-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.rev-slider-nav',
    autoplay: false,
    autoplaySpeed: 3000,
    cssEase: 'linear'
});
$('.rev-slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.rev-slider',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    centerPadding: '0',
    cssEase: 'linear'
});


// best images

$(document).ready(function () {
    $('.item-masonry').hover(
        function(){
            $(this).find('.grid-hover').css('display', 'flex');
        },
        function(){
            $(this).find('.grid-hover').css('display', 'none')
        }
    );
});


let $container = $('.gallery');

$container.imagesLoaded(function(){
    $container.masonry({
        itemSelector: '.item-masonry',
        columnWidth: '.sizer1',
        gutter: 10
    })
})

let loader2 = document.querySelector('.loader2')
let loadButton2 = document.querySelector('.load-more-button2');
loadButton2.addEventListener('click', (event)=> {
    event.preventDefault();
    loader2.classList.remove('hidden-loader2')
    event.target.parentNode.replaceChild(document.querySelector('.loader2'), loadButton2);
    setTimeout(function(){
        loader2.classList.add('hidden-loader2');
        let bestImages = document.querySelector('.gallery');
        let newGrid = document.createElement('div');
        newGrid.classList.add('gallery2');
        bestImages.after(newGrid);
        let $container2 = $('.gallery2');
        for(let i = 9; i <= 14; i++){
            newGrid.insertAdjacentHTML('afterbegin', `<div class="item-masonry sizer1"><img src="./IMG/gallery/gal${i}.png"><div class="grid-hover"><a href="#"><i class="fas fa-search"></i></a><a href=""><i class="fas fa-expand"></i></a></div></div>`);
        }
        $(document).ready(function () {
            $('.item-masonry').hover(
                function(){
                    $(this).find('.grid-hover').css('display', 'flex');
                },
                function(){
                    $(this).find('.grid-hover').css('display', 'none')
                }
            );
        });
        $container2.imagesLoaded(function(){
            $container2.masonry({
                itemSelector: '.item-masonry',
                columnWidth: '.sizer1',
                gutter: 10
            })
        })
    },2000)
})

const description = {
    webdesign: {
        text: "some text",
        img: "link"
    },
    
}